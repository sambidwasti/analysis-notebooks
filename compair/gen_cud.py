#!/usr/bin/env python
"""
This purpose of this script is to gather the L3 (CUD Compatible) files form each subsystem and
combine it to one.

"""
# Created by Sambid Wasti 3/23/22
# Version check. (check commit and compare for more details)
# s1* only deals with things in the directory Events
# s1.0a : It simply appended the files. It could have been executed if the file was missing.
# s1.0b : This is created to align event ids, so all 3 subsystem must present. Also need to check if the cud file present.
# s1.0c : Integrating the new updated format of CsI
# s1.0d : 05/10/22 Removed the CsI sipm data. Also added [...] for execution time of the script.
# s1.0e : 06/07/22 Storing the previous attributes in CUD.
# s1.0f : 07/08/22 Using eventdata/czt/cathode_digitime from the czt file for the Events/CZT/CathodeTime dataset


# TODO: Include the pair alignment algorithm to determine true path using CZT or CSI hits.

import argparse
import h5py
import numpy as np
import os

VERSION = 's1.0f'


def get_common_index(trk_evtid, czt_evtid, csi_evtid):
    """ Find the common index using the event id from three subsystems.

    Parameters
    ----------
    trk_evtid : Dataset of Event IDs from tracker
    czt_evtid : Dataset of Event IDs from CZT
    csi_evtid :Dataset of Event IDs from CSI

    Returns
    -------
        trk_cm_index: Intarr
            Array of common index selection for tracker
        czt_cm_index: Intarr
            Array of common index selection for Czt
        csi_cm_index: Intarr
            Array of common index selection for Csi
    """
    delme1, tra_in1, csi_in1 = np.intersect1d(trk_evtid,csi_evtid,
                                              return_indices=True) # common index between tra and csi
    delme2, tra_in2, czt_in1 = np.intersect1d(trk_evtid,czt_evtid,
                                              return_indices=True) # common index between tra and czt
    delme3, csi_in2, czt_in2 = np.intersect1d(csi_evtid,czt_evtid,
                                              return_indices=True) # common index between czt and csi

    trk_cm_index, delme4, delme5 = np.intersect1d(tra_in1, tra_in2,
                                                  return_indices=True)  # get the ARRAY of common index.
    csi_cm_index, delme6, delme7 = np.intersect1d(csi_in1, csi_in2,
                                                  return_indices=True)  # get the ARRAY of common index.
    czt_cm_index, delme8, delme9 = np.intersect1d(czt_in1, czt_in2,
                                                  return_indices=True)  # get the ARRAY of common index.

    return trk_cm_index, czt_cm_index, csi_cm_index


def write_trk(f_trk,trk_in,comp=True):
    """ Write the Tracker dataset.

    Reselect the common event id indices and write the tracker dataset.

    Parameters
    ----------
    f_trk : h5file obj
        tracker h5 file object.
    trk_in: Intarr
        array of common indices for tracker.
    comp: Boolean
        Compression Flag. Default = True
    """
    trk_grp = f_cud.create_group('Events/TKR')

    #get type.
    id_type = f_trk['tracker/event_id'].dtype
    time_type = f_trk['tracker/event_time'].dtype
    pulse_type = f_trk['tracker/pulse_height'].dtype
    energy_type = f_trk['tracker/energy'].dtype

    #define Shape
    trk_id_size = len(trk_in)
    trk_grp.create_dataset(name = 'EventID', data=f_trk['tracker/event_id'][...][trk_in], shape = (trk_id_size,),
                           dtype=id_type, compression=comp)
    trk_grp.create_dataset(name = 'EventTime', data=f_trk['tracker/event_time'][...][trk_in], shape = (trk_id_size,),
                           dtype=time_type, compression=comp)

    trk_grp.create_dataset(name = 'Pulseheight', data=f_trk['tracker/pulse_height'][...][trk_in,:,:,:], shape = (trk_id_size,10,2,192),
                           dtype=pulse_type,compression=comp)
    trk_grp.create_dataset(name = 'Energy', data=f_trk['tracker/energy'][...][trk_in,:,:,:], shape = (trk_id_size,10,2,192),
                           dtype=energy_type,compression=comp)

    # L3 level
    # Old Attributes
    temp_att_dict = dict(f_trk.attrs.items())  # save all the previous attributes.
    # Put old attributes.
    for keys in temp_att_dict:
        f_cud[f'Events/TKR/'].attrs[f'{keys}'] = temp_att_dict[f'{keys}']

def write_trk_new(f_trk,trk_in,comp=True):
    """ Write the Tracker dataset.

    Reselect the common event id indices and write the tracker dataset.

    Parameters
    ----------
    f_trk : h5file obj
        tracker h5 file object.
    trk_in: Intarr
        array of common indices for tracker.
    comp: Boolean
        Compression Flag. Default = True
    """
    #DELME:NOTE: This is to work for the newer energy format.
    trk_grp = f_cud.create_group('Events/TKR')

    #get type.
    id_type = f_trk['tracker/event_id'].dtype
    time_type = f_trk['tracker/event_time'].dtype
    pulse_type = f_trk['tracker/pulse_height'].dtype
    energy_type = f_trk['tracker/energy'].dtype

    #define Shape
    trk_id_size = len(trk_in)
    trk_grp.create_dataset(name = 'EventID', data=f_trk['tracker/event_id'][...][trk_in], shape = (trk_id_size,),
                           dtype=id_type, compression=comp)
    trk_grp.create_dataset(name = 'EventTime', data=f_trk['tracker/event_time'][...][trk_in], shape = (trk_id_size,),
                           dtype=time_type, compression=comp)

    trk_grp.create_dataset(name = 'Pulseheight', data=f_trk['tracker/pulse_height'][...][trk_in,:,:,:], shape = (trk_id_size,10,2,192),
                           dtype=pulse_type,compression=comp)
    
    # DELME:STEP:get the array index for Energy, where event id is common event id.
    hit_evt_id = f_trk['tracker/energy'][...][:,0] # we retrieve all the hit evt id from the trk nrg.
    delme, indices1, indices2 = np.intersect1d(trk_in, hit_evt_id, return_indices = True ) # common indices for hitevtid in indieces2

    filtered_nrg_arr =  f_trk['tracker/energy'][...][indices2,:] # selects the common event ids only
    trk_nrg_size = len(indices2)


    trk_grp.create_dataset(name = 'Energy', data=f_trk['tracker/energy'][...][indices2,:], shape = (trk_nrg_size,6),
                           dtype=energy_type, compression=comp)

    # Conserving attributes
    temp_att_dict = dict(f_trk.attrs.items())  # save all the previous attributes.
    for keys in temp_att_dict:
        f_cud[f'Events/TKR/'].attrs[f'{keys}'] = temp_att_dict[f'{keys}']


def write_czt(f_czt,czt_in,comp=True):
    """ Write CZT Data.

    Parameters
    ----------
    f_czt : file object
        CZT file object
    czt_in : Intarr
        CZT common index array
    comp : Bool
        Flag for compression. Default = True.
    """
    czt_grp = f_cud.create_group('Events/CZT')

    # Types
    zid_type = f_czt['eventdata/czt/event_id'].dtype
    ztime_type = f_czt['eventdata/czt/event_time'].dtype
    zas_temp_type = f_czt['eventdata/czt/asic_temperature'].dtype
    zan_pul_type = f_czt['eventdata/czt/anode_pulseheight'].dtype
    zan_time_type = f_czt['eventdata/czt/anode_digitime'].dtype
    zcat_pul_type = f_czt['eventdata/czt/cathode_pulseheight'].dtype
    zcat_time_type = f_czt['eventdata/czt/cathode_digitime'].dtype
    zpad_pul_type = f_czt['eventdata/czt/pad_pulseheight'].dtype
    zener_type = f_czt['eventdata/czt/energy'].dtype
    zpos_type = f_czt['eventdata/czt/position'].dtype
    zutc_type = f_czt['eventdata/czt/utc_corr'].dtype

    czt_id_size = len(czt_in)
    # Data
    czt_grp.create_dataset(name = 'EventID', data=f_czt['eventdata/czt/event_id'][...][czt_in], shape = (czt_id_size,),
                           dtype=zid_type, compression=comp)
    czt_grp.create_dataset(name = 'EventTime', data=f_czt['eventdata/czt/event_time'][...][czt_in], shape = (czt_id_size,),
                           dtype=ztime_type, compression=comp)
    czt_grp.create_dataset(name='ASICTemp', data=f_czt['eventdata/czt/asic_temperature'][...][czt_in,:], shape=(czt_id_size,16),
                           dtype=zas_temp_type, compression=comp)
    czt_grp.create_dataset(name='AnodePulseheight', data=f_czt['eventdata/czt/anode_pulseheight'][...][czt_in,:, :],
                           shape=(czt_id_size, 16,16), dtype=zan_pul_type, compression=comp)
    czt_grp.create_dataset(name='AnodeTime', data=f_czt['eventdata/czt/anode_digitime'][...][czt_in, :, :],
                           shape=(czt_id_size, 16, 16), dtype=zan_time_type, compression=comp)
    czt_grp.create_dataset(name='CathodePulseheight', data=f_czt['eventdata/czt/cathode_pulseheight'][...][czt_in, :, :],
                           shape=(czt_id_size, 16, 16), dtype=zcat_pul_type, compression=comp)
    czt_grp.create_dataset(name='CathodeTime', data=f_czt['eventdata/czt/cathode_digitime'][...][czt_in, :, :],
                           shape=(czt_id_size, 16, 16), dtype=zcat_time_type, compression=comp)
    czt_grp.create_dataset(name='PadPulseheight', data=f_czt['eventdata/czt/pad_pulseheight'][...][czt_in, :, :, :],
                           shape=(czt_id_size, 16, 16, 4), dtype=zpad_pul_type, compression=comp)
    czt_grp.create_dataset(name='Energy', data=f_czt['eventdata/czt/energy'][...][czt_in, :, :],
                           shape=(czt_id_size, 16, 16), dtype=zener_type, compression=comp)
    czt_grp.create_dataset(name='Position', data=f_czt['eventdata/czt/position'][...][czt_in, :, :, :],
                           shape=(czt_id_size, 16, 16, 3), dtype=zpos_type, compression=comp)
    czt_grp.create_dataset(name='CorUTC', data=f_czt['eventdata/czt/utc_corr'][...][czt_in],
                           shape=(czt_id_size,), dtype=zutc_type, compression=comp)

    # L1 level
    # Old Attributes
    temp_att_dict = dict(f_czt.attrs.items())  # save all the previous attributes.
    # Put old attributes.
    for keys in temp_att_dict:
        f_cud[f'Events/CZT/'].attrs[f'czt_{keys}'] = temp_att_dict[f'{keys}']


def write_csi(f_csi,csi_in,comp=True):
    """Write CSI data.

    Parameters
    ----------
    f_csi : file object
        CsI File object
    csi_in : intarr
        CsI Common index array.
    comp : Boolean
        Compression Flag. Default = True

    """
    csi_grp = f_cud.create_group('Events/CsI')

    # Get Type
    cid_type = f_csi["EventID/EventID/values"].dtype
    ctime_type = f_csi["time/UTC/values"].dtype
    cuse_pos_type = f_csi["use_pos/block0_values"].dtype
    cuse_erg_type = f_csi["use_erg/block0_values"].dtype
    #cuse_sipm_type = f_csi["use_sipm/block0_values"].dtype
    cerg_type = f_csi["erg/block0_values"].dtype
    cpos_type = f_csi["pos/block0_values"].dtype
    #csipm_type = f_csi["sipm/block0_values"].dtype


    #define Shape variable
    csi_id_size = len(csi_in)

    # Data
    evtid_grp = f_cud.create_group('Events/CsI/EventID')
    evtid_grp.create_dataset(name='EventID', data=f_csi['EventID/EventID/values'][...][csi_in],
                             shape=(csi_id_size,), dtype=cid_type, compression=comp)

    evttime_grp = f_cud.create_group('Events/CsI/time')
    evttime_grp.create_dataset(name='UTC', data=f_csi['time/UTC/values'][...][csi_in],
                               shape=(csi_id_size,), dtype=ctime_type, compression=comp)

    csi_grp.create_dataset(name='use_pos', data=f_csi["use_pos/block0_values"][...][csi_in],
                               shape=(csi_id_size, 30), dtype=cuse_pos_type, compression=comp)

    csi_grp.create_dataset(name='use_erg', data=f_csi[f"use_erg/block0_values"][...][csi_in],
                               shape=(csi_id_size, 30), dtype=cuse_erg_type, compression=comp)

    #csi_grp.create_dataset(name='use_sipm', data=f_csi[f"use_sipm/block0_values"][...][csi_in],
                                #shape=(csi_id_size, 60), dtype=cuse_sipm_type, compression=comp)

    csi_grp.create_dataset(name='erg', data=f_csi[f"erg/block0_values"][...][csi_in],
                           shape=(csi_id_size, 30), dtype=cerg_type, compression=comp)

    csi_grp.create_dataset(name='pos', data=f_csi[f"pos/block0_values"][...][csi_in],
                           shape=(csi_id_size, 30), dtype=cpos_type, compression=comp)

    #csi_grp.create_dataset(name='sipm', data=f_csi[f"sipm/block0_values"][...][csi_in],
                            #shape=(csi_id_size, 60), dtype=csipm_type, compression=comp)

    # L3 level
    # Old Attributes
    temp_att_dict = dict(f_csi.attrs.items())  # save all the previous attributes.
    # Put old attributes.
    for keys in temp_att_dict:
        f_cud[f'Events/CsI/'].attrs[f'csi_{keys}'] = temp_att_dict[f'{keys}']


def create_cudfile(trk_file="",  czt_file="", csi_file="", trg_file="",verbose=False, comp=True):
    """Create a CUD file by combining the subsystem L3 files.

    Parameters
    ----------
    trk_file : str
        Tracker L3 file.
    czt_file : str
        CZT L3 file
    csi_file : str
        CSI L3 file.
    trg_file : str
        Trigger File.
    verbose : Bool
        Verbose flag to see where the program is executing. Its a simple verbose with print statement and not with logger. Default = False
    comp: Bool
        Compression Flag. Default = True
    Returns
    -------

    """

    if verbose:
        print(f" Checking if the files are missing.")

    if trk_file=="" or czt_file =="" or csi_file=="" :
        print("Error : One of subsystem file is missing.")
    else:

        fname_tra = trk_file.split('/')[-1]
        fname_csi = csi_file.split('/')[-1]
        fname_czt = czt_file.split('/')[-1]
        fname_trg = trg_file.split('/')[-1]

        f_cud.attrs['cud_file_trk'] = fname_tra
        f_cud.attrs['cud_file_csi'] = fname_csi
        f_cud.attrs['cud_file_czt'] = fname_czt
        f_cud.attrs['cud_file_trg'] = fname_trg


        with h5py.File(trk_file, 'r') as f_trk:
            with h5py.File(csi_file, 'r') as f_csi:
                with h5py.File(czt_file, 'r') as f_czt:
                    trk_evtid = f_trk['tracker/event_id'][...]
                    czt_evtid = f_czt['eventdata/czt/event_id'][...]
                    csi_evtid = f_csi['EventID/EventID/values'][...]

                    if verbose:
                        print('---- Finding common event ids ----')

                    trk_cm_index, czt_cm_index, csi_cm_index = get_common_index(trk_evtid, czt_evtid, csi_evtid)

                    cud_grp = f_cud.create_group('Events')

                    if verbose:
                        print('---- Writing data for common event ids ----')
                        print(f" Common-Index :{len(trk_cm_index)} ")
                        print('---- Tracker Started ----')
                        print(f" No evt Trk :{len(trk_evtid)} ")


                    write_trk(f_trk,trk_cm_index, comp=comp)

                    if verbose:
                        print('---- Tracker Done ----')
                        print('---- Czt Started ----')
                        print(f" No evt Czt :{len(czt_evtid)} ")


                    write_czt(f_czt, czt_cm_index, comp = comp)

                    if verbose:
                        print('---- CZT Done ----')
                        print('---- CSI Started ----')
                        print(f" No evt CsI :{len(csi_evtid)} ")


                    write_csi(f_csi, csi_cm_index, comp=comp)

                    if verbose:
                        print('----CSI done ----')

                    # Attributes
                    # CUD level
                    f_cud.attrs['cud_nevt_trk'] = len(trk_evtid)
                    f_cud.attrs['cud_nevt_czt'] = len(czt_evtid)
                    f_cud.attrs['cud_nevt_csi'] = len(csi_evtid)
                    f_cud.attrs['cud_nevt_common'] = len(trk_cm_index)



        with h5py.File(trg_file,) as f_trg:
            cud_grp = f_cud.create_group('Summary')
            f_trg.copy(f_trg["/trigger"],f_cud["/Summary"], name='Trigger')

            # L3 level
            # Old Attributes
            temp_att_dict = dict(f_trg.attrs.items())  # save all the previous attributes.
            for keys in temp_att_dict:
                f_cud['Summary/Trigger'].attrs[f'trg_{keys}'] = temp_att_dict[f'{keys}']

        if verbose:
            print('----Trigger Done ----')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate CUD using L3 from subsystems")
    parser.add_argument("-v", action='store_true', help=" Verbose flag to monitor progress or to debug")
    parser.add_argument("--files", nargs=4, help="L4 files, Tracker, CZT, CSI and Trigger. If not defined, the filenames are asked during the run.")
    parser.add_argument("--outfile", help='Name of the CUD file. The extension of *.CUD.h5 is appended to the name. If not defined, tracker filename used.')

    args = parser.parse_args()

    comp = True

    if args.v:
        verbose_flag = True
    else:
        verbose_flag = False

    if args.files:
        trk_file = args.files[0]
        czt_file = args.files[1]
        csi_file = args.files[2]
        trg_file = args.files[3]
    else:
        trk_file = input("Drag or write the path of Tracker L3 file : ")
        czt_file = input("Drag or write the path of CZT L1 file: ")
        csi_file = input("Drag or write the path of CSI L3 file: ")
        trg_file = input("Drag or write the path of Trigger file: ")

    if verbose_flag:
        print('>>>> Input files <<<<<')
        print(f"TRK :{trk_file}")
        print(f"CSI :{csi_file}")
        print(f"CZT :{czt_file}")
        print(f"TRG :{czt_file}")

    if args.outfile:
        outfile = f"{args.outfile}.CUD.h5"
    else:
        outfile = f"{trk_file.split('_L3.h5')[0]}.CUD.h5"

    if verbose_flag:
        print('>>>>> Output File Name <<<<<')
        print(f"OUtput File: {outfile}")

    # delete the file if it exists. (h5 doesnt overwrite)
    if os.path.exists(outfile):
        os.remove(outfile)

    with h5py.File(outfile, "w") as f_cud:
        f_cud.attrs['gencud_version']=VERSION
        create_cudfile(trk_file = trk_file.rstrip(), czt_file = czt_file.rstrip(), csi_file=csi_file.rstrip(), trg_file=trg_file.rstrip(), verbose=verbose_flag, comp=comp)

    if verbose_flag:
        print('>>>>> End <<<<<')





