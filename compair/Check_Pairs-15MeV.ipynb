{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "af017ed9",
   "metadata": {},
   "source": [
    "# 15 MEV Pair Check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "9ce02aca",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import os\n",
    "import math\n",
    "import h5py\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import tracker_pipeline.conv_l2tol3 as tp\n",
    "np.set_printoptions(suppress=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "1e5d7744",
   "metadata": {},
   "outputs": [],
   "source": [
    "fdata_dict_s15 ={\n",
    "    'cosima' : '/Users/swasti/code/beamtest/Sims/15MeV/15MeVBeam-100000Trig.inc1.id1.sim',\n",
    "    'l2'     : '/Users/swasti/code/beamtest/Sims/15MeV/TKR_15MeVBeam-100000Trig.inc1.id1_sim.L2.h5',\n",
    "    'l3'     : '/Users/swasti/code/beamtest/Sims/15MeV/TKR_15MeVBeam-100000Trig.inc1.id1_sim.L3.h5', \n",
    "    'sim_h5' : '/Users/swasti/code/beamtest/Sims/15MeV/15MeVBeam-100000Trig.inc1.id1.sim.h5',\n",
    "    'desh_h5': '/Users/swasti/code/beamtest/Sims/15MeV/TKR_15MeVBeam-100000Trig.inc1.id1_sim.h5',\n",
    "    'l1'     : '/Users/swasti/code/beamtest/Sims/15MeV/TKR_15MeVBeam-100000Trig.inc1.id1_sim.L1.h5',\n",
    "\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "efd4ab50",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = 's15'\n",
    "\n",
    "if run == 's15':\n",
    "    fdata_dict = fdata_dict_s15\n",
    "elif run == 's20':\n",
    "    fdata_dict = fdata_dict_s20"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58aaa617",
   "metadata": {},
   "source": [
    "## Quick Test : Between L2 and some of the sim formats."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "80ac1524",
   "metadata": {},
   "outputs": [],
   "source": [
    "fname = fdata_dict['l2']\n",
    "l2file = h5py.File(fname,'r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "12fae002",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<KeysViewHDF5 ['Calibration', 'DEE Version', 'Header', 'layer00', 'layer01', 'layer02', 'layer03', 'layer04']>"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l2file.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "468ef6bd",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<HDF5 dataset \"nrg\": shape (100001, 2, 192), type \"<f8\">"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l2file['layer00/vdata/nrg']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41d9c042",
   "metadata": {},
   "source": [
    "==== DESH One ===="
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "47b10595",
   "metadata": {},
   "outputs": [],
   "source": [
    "fname = fdata_dict['desh_h5']\n",
    "deshfile = h5py.File(fname,'r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "2bb239df",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<KeysViewHDF5 ['Energy', 'event_id', 'running_time']>"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "deshfile['TKR'].keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "2bbc99a7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<HDF5 dataset \"Energy\": shape (100000, 10, 2, 192), type \"<f8\">"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "deshfile['TKR/Energy']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5ae0dbb8",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "6d6cffa3",
   "metadata": {},
   "source": [
    "# Cosima file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "5490dcd8",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(fdata_dict['cosima']) as f:\n",
    "    contents = f.readlines()\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "f5ce1149",
   "metadata": {},
   "outputs": [],
   "source": [
    "trk_hit_main =[]\n",
    "for i in range(len(contents)):\n",
    "# for i in range(400):\n",
    "    line = contents[i]\n",
    "\n",
    "    if not line.isspace():\n",
    "        if (line.find('SE') >= 0):\n",
    "            \n",
    "            line = contents[i+1]\n",
    "            curid = line.split()[1] #next line to SE is is the ID.\n",
    "            \n",
    "#             print(curid)\n",
    "\n",
    "            pair_flag = False\n",
    "            trk_flag = False\n",
    "            \n",
    "            trk_hit = []\n",
    "            \n",
    "            i+=1\n",
    "            line = contents[i]\n",
    "            while (line.find('SE') < 0):          \n",
    "                \n",
    "                if (line.find('IA') >= 0) and  (line.find('PAIR') >= 0):  # no. of hits\n",
    "                    pair_flag = True\n",
    "\n",
    "                if (line.find('HTsim') >= 0):  # no. of hits\n",
    "                    delme = line.split()\n",
    "                    if delme[1] == '1;':\n",
    "                        delme1 = line.split(\";\")\n",
    "                        trk_hit = [float(curid),float(delme1[1]),float(delme1[2]),float(delme1[3]),float(delme1[4])]\n",
    "                        trk_flag = True\n",
    "                        \n",
    "                        if pair_flag:\n",
    "                            trk_hit_main.append(trk_hit)\n",
    "                \n",
    "                \n",
    "                    \n",
    "                i+=1\n",
    "                if i<len(contents):\n",
    "                    line = contents[i]\n",
    "                else:\n",
    "                    break\n",
    "            "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "1ae72269",
   "metadata": {},
   "outputs": [],
   "source": [
    "#X = data[:, [1, 9]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "491b530f",
   "metadata": {},
   "outputs": [],
   "source": [
    "trk_hit_main = np.array(trk_hit_main)\n",
    "trk_evtid = trk_hit_main[:,0] # Tracks only pair"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "6f84ff81",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "31451\n"
     ]
    }
   ],
   "source": [
    "print(len(np.unique(trk_evtid)))\n",
    "pair_evtid= np.unique(trk_evtid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "bd457807",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[1381.     ,   -0.0765 ,    0.6375 ,    3.825  ,  148.33493],\n",
       "       [1381.     ,    0.1275 ,    0.5865 ,    3.825  ,   13.78401],\n",
       "       [1381.     ,   -0.0255 ,    0.8925 ,    3.825  ,  148.31339],\n",
       "       [1381.     ,   -0.2295 ,   -0.1785 ,    1.925  ,  160.31704],\n",
       "       [1381.     ,   -0.2805 ,   -0.2295 ,    1.925  ,   24.84143],\n",
       "       [1381.     ,   -1.1475 ,   -0.1785 ,    0.025  ,  105.27233],\n",
       "       [1381.     ,   -1.1985 ,   -0.1785 ,    0.025  ,   76.42496],\n",
       "       [1381.     ,   -1.3515 ,    0.4845 ,    0.025  ,  446.38455],\n",
       "       [1381.     ,   -1.3515 ,    2.7285 ,    0.025  ,   30.07146],\n",
       "       [1381.     ,   -0.5355 ,    1.5045 ,    1.925  ,  222.98258]])"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "trk_hit_main[np.where(trk_hit_main[:,0]==1381)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4ac058a",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "9c4823a9",
   "metadata": {},
   "source": [
    "# Sim h5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "a80dcef0",
   "metadata": {},
   "outputs": [],
   "source": [
    "fname = fdata_dict['sim_h5']\n",
    "h5_sim = h5py.File(fname,'r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "f4591da7",
   "metadata": {},
   "outputs": [],
   "source": [
    "h5_nrg = h5_sim['Events/TKR/Energy']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "a7dbba86",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([1, 2, 3, 4, 5])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "h5_sim['Events/TKR/event_id'][0:5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "8a70e5e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "nevt = h5_nrg.shape[0]\n",
    "cnt_array=np.zeros((10,6,6), dtype=int)\n",
    "\n",
    "for i in range(nevt):\n",
    "    t_nrg = h5_nrg[i]\n",
    "    \n",
    "    for j in range(10):\n",
    "        temp_nrg = t_nrg[j]\n",
    "        \n",
    "        a_cnt = np.count_nonzero(temp_nrg[0,:]>0.0)  # side a count\n",
    "        b_cnt = np.count_nonzero(temp_nrg[1,:]>0.0)  # side b count\n",
    "        \n",
    "        if a_cnt>4 or b_cnt>4:\n",
    "            cnt_array[j,5,5]+= 1\n",
    "        else:\n",
    "            cnt_array[j,a_cnt,b_cnt]+= 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "b3873c2a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[901968,      0,      0,      0,      0,      0],\n",
       "       [     0,  37369,  11228,    570,    109,      0],\n",
       "       [     0,  11342,  11445,   3826,    897,      0],\n",
       "       [     0,    580,   3935,   3786,   1724,      0],\n",
       "       [     0,     87,    876,   1692,   1326,      0],\n",
       "       [     0,      0,      0,      0,      0,   7240]])"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "simh5_tothitarr = np.sum(cnt_array, axis=0)\n",
    "simh5_tothitarr"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10a3b8c1",
   "metadata": {},
   "source": [
    "# Sim L2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "3dcc13ea",
   "metadata": {},
   "outputs": [],
   "source": [
    "fname = fdata_dict['l2']\n",
    "l2_ns = h5py.File(fname,'r')\n",
    "layer_list = ['layer00','layer01','layer02','layer03','layer04','layer05','layer06','layer07','layer08','layer09']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "1af9870e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<HDF5 dataset \"sync_index\": shape (100001,), type \"<i8\">"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l2_ns['layer00/data/sync_index']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "43f708a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "evt = l2_ns['layer00/vdata/nrg'].shape[0]\n",
    "l2_ns_nrg = np.zeros(shape=(evt,10,2,192))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "6ecbecd2",
   "metadata": {},
   "outputs": [],
   "source": [
    "for l in range(10):\n",
    "    if layer_list[l] in l2_ns.keys():\n",
    "        l2_ns_nrg[:,l,:,:] = l2_ns[f'{layer_list[l]}/vdata/nrg'][...]\n",
    "# l2_ns_nrg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "6ec8ba24",
   "metadata": {},
   "outputs": [],
   "source": [
    "nevt1 = l2_ns_nrg.shape[0]\n",
    "cnt_array1 =np.zeros((10,6,6), dtype=int)\n",
    "\n",
    "for i in range(nevt):\n",
    "    t_nrg = l2_ns_nrg[i]\n",
    "    \n",
    "    for j in range(10):\n",
    "        temp_nrg =t_nrg[j]\n",
    "        \n",
    "        a_cnt = np.count_nonzero(temp_nrg[0,:]>0.0)  # side a count\n",
    "        b_cnt = np.count_nonzero(temp_nrg[1,:]>0.0)  # side b count\n",
    "        \n",
    "        if a_cnt>4 or b_cnt>4:\n",
    "            cnt_array1[j,5,5]+= 1\n",
    "        else:\n",
    "            cnt_array1[j,a_cnt,b_cnt]+= 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "1ea34ed9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[904438,   2977,    503,     44,     17,      0],\n",
       "       [ 11922,  38388,   6058,    553,    109,      0],\n",
       "       [  2750,   8665,   7505,   2133,    474,      0],\n",
       "       [   498,   1567,   2588,   1884,    742,      0],\n",
       "       [   117,    436,    740,    802,    569,      0],\n",
       "       [     0,      0,      0,      0,      0,   3521]])"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l2_tothitarr = np.sum(cnt_array1, axis=0)\n",
    "l2_tothitarr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "84c6d5b3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(100001, 10, 2, 192)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l2_ns_nrg.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "45ab5ff8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([255.19317, 255.28056, 184.64488, ..., 125.63568, 233.88737,\n",
       "       233.85156])"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l2_ns_nrg_arr = l2_ns_nrg[l2_ns_nrg>0.0]\n",
    "l2_ns_nrg_arr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "51641086",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(l2_ns_nrg_arr, bins=50)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bff2556",
   "metadata": {},
   "source": [
    "\n",
    "## compare (transition of types from sim2h5 --> L2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "5503c089",
   "metadata": {},
   "outputs": [],
   "source": [
    "tot_test_arr = np.subtract(l2_tothitarr,simh5_tothitarr)\n",
    "tot_test_arr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "2cc3c2d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.sum(tot_test_arr)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc7d9abf",
   "metadata": {},
   "source": [
    "# Sim L3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "31b5b825",
   "metadata": {},
   "outputs": [],
   "source": [
    "fname = fdata_dict['l3']\n",
    "l3_ns = h5py.File(fname,'r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "23fd6ee2",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<KeysViewHDF5 ['energy', 'event_id', 'event_time', 'pulse_height']>"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l3_ns['tracker'].keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "825375ba",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[  1.      ,  -0.3315  ,  -0.7395  ,   0.025   , 184.64488 ,\n",
       "          1.      ],\n",
       "       [  1.      ,  -0.1275  ,  -0.4845  ,   1.925   , 255.236865,\n",
       "          1.      ],\n",
       "       [ 10.      ,   0.1785  ,   0.5355  ,   1.925   , 146.45956 ,\n",
       "          3.      ],\n",
       "       [ 10.      ,  -0.5355  ,   0.4335  ,   1.925   , 140.22798 ,\n",
       "          3.      ],\n",
       "       [ 10.      ,   0.1785  ,   0.4335  ,   1.925   , 146.45956 ,\n",
       "          2.      ],\n",
       "       [ 10.      ,  -0.5355  ,   0.5355  ,   1.925   , 140.22798 ,\n",
       "          2.      ],\n",
       "       [ 10.      ,   0.663   ,   2.193   ,   0.025   , 384.88869 ,\n",
       "          1.      ],\n",
       "       [ 12.      ,   1.3005  ,  -0.6375  ,   0.025   , 173.741225,\n",
       "          1.      ],\n",
       "       [ 17.      ,   0.4335  ,   0.0255  ,   5.725   , 144.0664  ,\n",
       "          1.      ],\n",
       "       [ 17.      ,   0.2295  ,  -0.2805  ,   7.625   , 209.29632 ,\n",
       "          2.      ]])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l3_ns['tracker/energy'][0:10]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c938c3c1",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "9b8c2c72",
   "metadata": {},
   "outputs": [],
   "source": [
    "l3_ns_nrg = l3_ns['tracker/energy'][...]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7b821f9",
   "metadata": {},
   "source": [
    "# Check Pair at different levels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "id": "87b0dd2e",
   "metadata": {},
   "outputs": [],
   "source": [
    "pair_evt = pair_evtid[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c985036",
   "metadata": {},
   "source": [
    "## Check at sim"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "id": "dc54bb7d",
   "metadata": {},
   "outputs": [],
   "source": [
    "trk_hit_main[np.where(trk_hit_main[:,0] == pair_evt)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d886a1b",
   "metadata": {},
   "source": [
    "## check at simh5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "id": "4cd197cc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note that some of these, converts to 2x1y, etc gets added.\n",
    "t_nrg = h5_nrg[32,:,:,:]\n",
    "t_nrg[np.where(t_nrg[:,:,:]>0)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "id": "2a8615ac",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "np.where(t_nrg[:,:,:]>0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "id": "e5ebd9f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "t_nrg.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2302ff8",
   "metadata": {},
   "source": [
    "## check at L2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "id": "3e12b585",
   "metadata": {},
   "outputs": [],
   "source": [
    "t_nrg = l2_ns_nrg[1381,:,:,:]\n",
    "t_nrg[np.where(t_nrg[:,:,:]>0)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "id": "ab50b989",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.where(t_nrg[:,:,:]>0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a69021bc",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d860befc",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "0238e6a8",
   "metadata": {},
   "source": [
    "## Check at L3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "id": "1518ffee",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.where(t_nrg[:,:,:]>0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b93d366",
   "metadata": {},
   "source": [
    "## Various Checks"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18eabcb2",
   "metadata": {},
   "source": [
    "### check at L2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "id": "e25f38d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "t_nrg[6,:,:]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2da011e9",
   "metadata": {},
   "source": [
    "### Check L1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 77,
   "id": "2e1b9838",
   "metadata": {},
   "outputs": [],
   "source": [
    "fname = fdata_dict['l1']\n",
    "l1_ns = h5py.File(fname,'r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 78,
   "id": "f41ac050",
   "metadata": {},
   "outputs": [],
   "source": [
    "l1_ns.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 79,
   "id": "a304f674",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.array(l1_ns['layer09/vdata/channel_cm_sub'][4])\n",
    "np.where( np.array(l1_ns['layer09/vdata/channel_cm_sub'][4]) > 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4abeb47",
   "metadata": {},
   "source": [
    "### RevCal desh file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 80,
   "id": "3d952bf2",
   "metadata": {},
   "outputs": [],
   "source": [
    "fname = fdata_dict['ns_desh']\n",
    "desh_ns = h5py.File(fname,'r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "id": "2a582699",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.array(desh_ns['TKR/Energy'][3,9,:,:])\n",
    "np.where(np.array(desh_ns['TKR/Energy'][3,9,:,:]) >0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ad93491",
   "metadata": {},
   "source": [
    "# Statistics"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9d9221d",
   "metadata": {},
   "source": [
    "## Check the 2hit L3s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "id": "f5520cf8",
   "metadata": {},
   "outputs": [],
   "source": [
    "l3_ns_nrg[:,5] > 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "76053798",
   "metadata": {},
   "outputs": [],
   "source": [
    "def_pair = l3_ns_nrg[l3_ns_nrg[:,5]>1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "21659450",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "20168"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dp_id = def_pair[:,0]\n",
    "len(dp_id)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "9bb5cd9b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(4924,)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "uq_dp_id= np.unique(dp_id)\n",
    "uq_dp_id.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81655c55",
   "metadata": {},
   "source": [
    "> No. of events with 2x2 combinations 13763"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "id": "cbf0b3c7",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "trk_hit_main,trk_evtid"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b99ace95",
   "metadata": {},
   "source": [
    "## for each of the unique ID, check if that hit is correct in sim.\n",
    "\n",
    "> Steps\n",
    "* get hits for the event id from L3\n",
    "* get the same hit info from sim\n",
    "* compare the L3 hit with qflag 2 with sim hit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "67aa5383",
   "metadata": {},
   "outputs": [],
   "source": [
    "NLAYERS = 5\n",
    "#---- Position Constansts ----\n",
    "TKR_STRIP_PITCH     = 0.051                             # [cm] 510 microns\n",
    "TKR_WIDTH           = 192 * TKR_STRIP_PITCH             # [cm] 192 strips of constant pitch\n",
    "TKR_EDGE            = TKR_WIDTH/2.0                     # DSSD is centered at 0, thus the edges are equivalent to half of the width\n",
    "TKR_STRIP_CENTROID  = TKR_EDGE - (TKR_STRIP_PITCH)/2.0  # Gives the center of strip 191\n",
    "\n",
    "TRK_LAYER_THICKNESS = 0.05 \n",
    "\n",
    "TKR_LAYER_SPACING   = 1.9      \n",
    "# [cm] z spacing between layers\n",
    "TKR_TOP_LAYER       = (TRK_LAYER_THICKNESS/2.0) + (NLAYERS - 1) * TKR_LAYER_SPACING # [cm] Global z position of layer 0 of the TKR \n",
    "\n",
    "\n",
    "def TKR_pinpoint(xpos, ypos, zpos):\n",
    "\n",
    "    xstrip = round((float(xpos)+TKR_STRIP_CENTROID)/TKR_STRIP_PITCH)\n",
    "    ystrip = round(-(float(ypos)-TKR_STRIP_CENTROID)/TKR_STRIP_PITCH)\n",
    "    zlayer = round(abs((float(zpos)-TKR_TOP_LAYER)/TKR_LAYER_SPACING))\n",
    "    # print('X-strip # ' + str(xstrip) + '; Y-strip # ' + str(ystrip) + '; Z-layer # ' + str(zlayer))\n",
    "    return xstrip, ystrip, zlayer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "7fc19537",
   "metadata": {},
   "outputs": [],
   "source": [
    "def check_hits(sim_array,hit_array):\n",
    "    n_sim = sim_array.shape[0]\n",
    "    n_l3 = hit_array.shape[0]\n",
    "    \n",
    "    ret_list = []\n",
    "    for i in range(n_l3):\n",
    "        tr_flg = False\n",
    "        \n",
    "        for j in range(n_sim):\n",
    "#             print(sim_array[j,1], hit_array[i,1])\n",
    "            if (sim_array[j,1] == hit_array[i,1])\\\n",
    "                 and sim_array[j,2] == hit_array[i,2] and \\\n",
    "                 sim_array[j,3] == hit_array[i,3] and abs(sim_array[j,4]-hit_array[i,4])<2.0:\n",
    "                \n",
    "                 tr_flg = True\n",
    "        \n",
    "        if tr_flg == False:\n",
    "#             print(check_nbr(sim_array,hit_array[i]),'***')\n",
    "            tr_flg = check_nbr(sim_array,hit_array[i])\n",
    "        \n",
    "        ret_list.append(tr_flg)\n",
    "    return ret_list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "ca42d952",
   "metadata": {},
   "outputs": [],
   "source": [
    "# here\n",
    "def check_nbr(sim_array, hit_array):\n",
    "    global check_flag\n",
    "    \n",
    "    t_sim_array = sim_array[np.where(sim_array[:,3]==hit_array[3])] # zselection.\n",
    "    n_sim = t_sim_array.shape[0] # no. of sim\n",
    "    all_nrg = t_sim_array[:,4]\n",
    "    all_x = np.round(t_sim_array[:,1],5)\n",
    "    all_y = t_sim_array[:,2]\n",
    "    \n",
    "    hit_flag = False\n",
    "    \n",
    "    #Check for X neighbor\n",
    "    com_x = np.zeros((len(all_x),len(all_x)),dtype=bool)\n",
    "    com_y = np.zeros((len(all_y),len(all_y)),dtype=bool)\n",
    "    \n",
    "    x_gen = np.zeros((len(all_x),4),dtype=float)\n",
    "    y_gen = np.zeros((len(all_y),4),dtype=float)\n",
    "    for i in range(len(all_x)):\n",
    "        tflag_arr=[]\n",
    "        for j in range(len(all_x)):\n",
    "            tflag= False\n",
    "            if abs(all_x[i]==all_x[j]):\n",
    "                tflag = True\n",
    "            tflag_arr.append(tflag)\n",
    "        \n",
    "        #have array of neighbors or not. \n",
    "        \n",
    "        n_neighbors = np.count_nonzero(np.array(tflag_arr)==True)\n",
    "        new_y = np.round(np.sum(all_y[tflag_arr])/n_neighbors,5)\n",
    "        new_x = all_x[tflag_arr][0]\n",
    "        new_z = hit_array[3]\n",
    "        new_nrg = np.sum(all_nrg[tflag_arr])\n",
    "        \n",
    "        com_x[i] = np.array(tflag_arr)\n",
    "        x_gen[i] = np.array([new_x,new_y,new_z,new_nrg])\n",
    "        print(new_x,new_y,new_z,new_nrg,'<<<') if check_flag else None\n",
    "        print(tflag_arr,'<<<') if check_flag else None\n",
    "        \n",
    "        \n",
    "        \n",
    "        #check for hit flag.\n",
    "        if (new_x == hit_array[1])\\\n",
    "             and new_y == hit_array[2] and \\\n",
    "                     new_z == hit_array[3] and abs(new_nrg-hit_array[4])<2:\n",
    "                            hit_flag =True\n",
    "    \n",
    "    if hit_flag == True:\n",
    "        return True\n",
    "    #Check for Y neighbor\n",
    "    for i in range(len(all_y)):\n",
    "        tflag_arr=[]\n",
    "        for j in range(len(all_y)):\n",
    "            tflag= False\n",
    "            if abs(all_y[i]==all_y[j]):\n",
    "                tflag = True\n",
    "            tflag_arr.append(tflag)\n",
    "        \n",
    "        #have array of neighbors or not. \n",
    "            \n",
    "        n_neighbors = np.count_nonzero(np.array(tflag_arr)==True)\n",
    "        new_x = np.round(np.sum(all_x[tflag_arr])/n_neighbors,5)\n",
    "        new_y = all_y[tflag_arr][0]\n",
    "        new_z = hit_array[3]\n",
    "        new_nrg = np.sum(all_nrg[tflag_arr])\n",
    "                \n",
    "        com_y[i] = np.array(tflag_arr)\n",
    "        y_gen[i] = np.array([new_x,new_y,new_z,new_nrg])\n",
    "        #check for hit flag.\n",
    "        if (new_x == hit_array[1])\\\n",
    "             and new_y == hit_array[2] and \\\n",
    "                     new_z == hit_array[3] and abs(new_nrg-hit_array[4])<2:\n",
    "                            hit_flag =True\n",
    "        print(new_x,new_y,new_z,new_nrg,'>>>>') if check_flag else None\n",
    "        print(tflag_arr,'>>>>>') if check_flag else None\n",
    "        if hit_flag == True:\n",
    "            return True\n",
    "        \n",
    "\n",
    "    \n",
    "    for i in range(com_y.shape[0]):\n",
    "        if y_gen[i,0]== hit_array[1]:\n",
    "            if x_gen[i,1] == hit_array[2]:\n",
    "                t_nrg = x_gen[i,3]+y_gen[i,3]-all_nrg[i]\n",
    "                if abs(t_nrg-hit_array[4])<2:\n",
    "                    hit_flag = True\n",
    "                print( i, y_gen[i,0], x_gen[i,1],t_nrg) if check_flag else None\n",
    "                \n",
    "    return hit_flag\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "10fbf62d",
   "metadata": {},
   "outputs": [],
   "source": [
    "check_flag = False\n",
    "check_i = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "aa34fd34",
   "metadata": {},
   "outputs": [],
   "source": [
    "# for i in range(len(uq_dp_id)):\n",
    "\n",
    "cnt_tt = 0\n",
    "cnt_bb = 0\n",
    "cnt_ff = 0\n",
    "cnt_tf = 0\n",
    "cnt_wp = 0\n",
    "cnt_tff = 0\n",
    "# for i in range(100,500):\n",
    "for i in range(len(uq_dp_id)):\n",
    "    tid = uq_dp_id[i]\n",
    "    \n",
    "    \n",
    "    if check_flag and i !=check_i:\n",
    "        continue \n",
    "     \n",
    "    t_evt_l3 = np.round(def_pair[np.where(dp_id == tid)],5)\n",
    "    t_evt_sim= trk_hit_main[np.where(trk_evtid == tid)]\n",
    "    \n",
    "    if check_flag:\n",
    "        print('==')\n",
    "        print(i)\n",
    "#         print(t_evt_l3)\n",
    "        print('--')\n",
    "        print(t_evt_sim)\n",
    "\n",
    "          \n",
    "    if t_evt_sim.shape[0]== 0:\n",
    "        cnt_wp+=1\n",
    "        print(i,'WP', cnt_wp) if check_flag else None\n",
    "        continue\n",
    "    #goodhits:\n",
    "    t_evt_l3_gd = t_evt_l3[t_evt_l3[:,5]==2]\n",
    "    checks = check_hits(t_evt_sim , t_evt_l3_gd)\n",
    "    print(t_evt_l3_gd) if check_flag else None \n",
    "#     print(i, checks) \n",
    "          \n",
    "    if checks[0] == True and checks[1]==True:\n",
    "        cnt_tt+=1\n",
    "        print('TT', cnt_tt) if check_flag else None\n",
    "        \n",
    "    elif checks[0] == False and checks[1]==False:\n",
    "        cnt_tff+=1\n",
    "        t_evt_l3_bd = t_evt_l3[t_evt_l3[:,5]==3]\n",
    "        checks1 = check_hits(t_evt_sim , t_evt_l3_bd)\n",
    "        print(checks1) if check_flag else None\n",
    "        if checks1[0] == True and checks1[1]==True:\n",
    "            cnt_bb+=1 \n",
    "            print(i,'BB')if check_flag else None\n",
    "            \n",
    "        elif checks1[0] == True or checks1[1]==True:\n",
    "            cnt_tf += 1\n",
    "            print(i,'TFb')if check_flag else None\n",
    "        else:\n",
    "            cnt_ff +=1\n",
    "            print(i,'FFb')if check_flag else None\n",
    "            \n",
    "    elif checks[0] == True or checks[1] == True:\n",
    "        cnt_tf +=1\n",
    "        print(i,'TFa',cnt_tf) if check_flag else None\n",
    "    else:\n",
    "        cnt_ff+=1\n",
    "        print(i,'FFa')if check_flag else None\n",
    "    print('===') if check_flag else None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "77a3383a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2169, 680, 0, 1084, 991, 714)"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cnt_tt, cnt_ff, cnt_bb, cnt_tf, cnt_wp, cnt_tff"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "54a8331e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4924"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cnt_tt+cnt_ff+cnt_bb+cnt_tf+cnt_wp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47c2b1d8",
   "metadata": {},
   "source": [
    "10 MeV      \n",
    "* 18426\n",
    "\n",
    "\n",
    "20 Mev        \n",
    "\n",
    "Modified for cases where adding a1,b1, a2,b1 and a2b2 and had to add all 3.        \n",
    "13781\n",
    "(10319, 80, 1, 865, 2516, 83)\n",
    "\n",
    "Modified to add for same hit:\n",
    "(8810, 96, 1, 2344, 2509)\n",
    "\n",
    "---\n",
    "(6076, 1380, 0, 3795, 2509) = 13760         \n",
    "Pie Chart/ Sankey Plot         \n",
    "        \n",
    "44784            \n",
    "\n",
    "(6076, 1380, 0, 3795) = 11251\n",
    "(6085, 1376, 0, 3804, 2516, 1376)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "id": "eaafedb8",
   "metadata": {},
   "outputs": [],
   "source": [
    "TKR_pinpoint(1.5045 ,  -0.1275 ,  13.325)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "id": "48416328",
   "metadata": {},
   "outputs": [],
   "source": [
    "t_nrg = l2_ns_nrg[142,:,:,:]\n",
    "t_nrg[np.where(t_nrg[:,:,:]>0)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 185,
   "id": "2a72f62c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(array([], dtype=int64), array([], dtype=int64), array([], dtype=int64))"
      ]
     },
     "execution_count": 185,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.where(t_nrg[:,:,:]>0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "id": "71b3bbd1",
   "metadata": {},
   "outputs": [],
   "source": [
    "t_nrg1= h5_nrg[1381,:,:,:]\n",
    "t_nrg1[np.where(t_nrg1[:,:,:]>0)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 187,
   "id": "8e30e1ff",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(array([], dtype=int64), array([], dtype=int64), array([], dtype=int64))"
      ]
     },
     "execution_count": 187,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.where(t_nrg1[:,:,:]>0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c96527f4",
   "metadata": {},
   "source": [
    "# A lot of events are not in L2 pairs. What are these events like?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99870170",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9141c691",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8fc84992",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f92feb3",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bba0f740",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce089b27",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d5a98546",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.11"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "302px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
