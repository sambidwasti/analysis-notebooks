#!/usr/bin/env python
""" This is script used to convert a given CUD h5 to Revan Readable (RR) format. This is a modified version of CUD2RR.

    The Revan readable format is simply a record of each event interaction positon and energy (which also defines the
    detector type). So this script, selects each event id, extracts the information from CUD h5 and rewrites in the
    MEGAlib language that Revan understands.


    usage

    >>> python cud2rr file_path

"""

# NOTE:REVIEW:
# The revan readable is the sim file (we name it evta) that cosima can read.
# STEPS:
# Basic workflow:
#       NOTE: CUD does not select common event ids anymore
#       1. We get the unique event ids (union) from all subsystems. 
#       2. We loop through each event id to get hits.
#           2.1: We verify if that event id present in the nrg eventid.
#           2.2: For each event id, we grab the energy and position info.
#          
#       And redefine that as hit info for Revan readable.

# Versions
# 2.0a fixed units in Csi.
# 2.1 fixing CZT and modifying for non-common event ids.
# 3.0 : Sambid Wasti 030823
#       - CUD has new format for tracker for hits
#       - CZT has new position calibration.




import numpy as np
import h5py
import time
import argparse
import sys

# Global Variables.

# IV.1) TKR
CUD2RR_VERSION = '3.0' #
MEGA_VERSION = 101 # This is the MEGALib version.

# NOTE:Units are in cm
# This is not needed anymore.
TKR_LAYER_NUMBER = 7
TKR_LAYER_SPACING = 1.9
TKR_LAYER_Z_OFFSET = 0.025
TKR_STRIP_PITCH = 0.051 # 510 microns (wide/thickness)
TKR_WIDTH = 192 * TKR_STRIP_PITCH # 192 strips of constant pitch
TKR_EDGE = TKR_WIDTH/2 # DSSD is centered at 0, thus the edges are equivalent to half of the width
TKR_STRIP_CENTROID = TKR_EDGE - (TKR_STRIP_PITCH)/2 # Gives the center of strip 191


# IV.2) CsI

#CsI_top_centroid = -20.641 # Global z position of the top layer of the CsI (centroid)
#CsI_layer_spacing = 1.9 # z spacing between layers, center to center of crystal
#CsI_x_centroid = 4.25
#CsI_y_centroid = 4.25

# IV.3 CZT

CZT_edge = 5 # CZT is 10x10, edge is at 5 (center of CZT = 0)
CZT_edge_midpoint = 3.75 # The center of the blocks nearest the edge
CZT_block_spacing = 2.5 # Each block is 2.5 wide
CZT_bar_edge = 1.25 # Each block has a width of 2.5; center at 0 -> ranges from -1.25:1.25
CZT_bar_spacing = 0.625 # Each bar is 0.625 wide
CZT_bar_edge_mid = 0.625 # The center of the bars nearest the edge of the block

# Software Threshold (kev)
TRK_SOFT_THRS = 40.00
CSI_SOFT_THRS = 40.00
CZT_SOFT_THRS = 40.00

def get_tracker_hits(tkr_ene_arr):
    """ Function to take in the Tracker Energy Array for an event and return hit information.

    This is called for each event id. The bottom layer defines the z origin. But the top
    layer is layer 0. The position are defined by the detector parameters (global parameters)
    with the origin (x,y) being the center.

    Parameters
    ----------
    tkr_ene_arr : Intarr
        Tracker Energy Array. Shape of this array is (10,2,192).

    Returns
    -------
        hit_tkr_ene : np.array
            Array of Tracker Hit Energy.
        hit_tkr_xpos : np.array
            Array of Tracker Hit Xposition.
        hit_tkr_ypos : np.array
            Array of Tracker Hit Yposition.
        hit_tkr_zpos : np.array
            Array of Tracker Hit Zposition.
    """

    hit_tkr_ene = []
    hit_tkr_xpos = []
    hit_tkr_ypos = []
    hit_tkr_zpos = []
    [a,b,c] = tkr_ene_arr.shape
    for layer in np.arange(a):

        if layer > (TKR_LAYER_NUMBER-1): # added to skip layer greater than 6 (7th layer)
            continue

        temp_ene = []
        temp_x = []    
        temp_y = []
        temp_z = []
        for orientation in np.arange(b):
            for strip in np.arange(c):
                if tkr_ene_arr[layer,orientation,strip] > TRK_SOFT_THRS:
                    ene = tkr_ene_arr[layer,orientation,strip]
                    if orientation == 0:
                        xpos = float(strip*TKR_STRIP_PITCH - TKR_STRIP_CENTROID)
                        ypos = 0
                    if orientation == 1:
                        xpos = 0
                        ypos = float(-strip*TKR_STRIP_PITCH + TKR_STRIP_CENTROID)

                    l_pos = layer *TKR_LAYER_SPACING+ TKR_LAYER_Z_OFFSET
                    zpos =  float(l_pos)

                    temp_ene.append(float(ene))
                    temp_x.append(float(xpos))
                    temp_y.append(float(ypos))
                    temp_z.append(float(zpos))
        if len(temp_ene) == 0:
            continue
        if len(temp_ene)%2 == 1:
            print('Skipped layered event data due to odd number of strip hits')
            print(temp_ene)
            continue
        idx_sort = np.argsort(temp_ene)
        temp_ene = np.array(temp_ene)[idx_sort]
        
        temp_x = np.array(temp_x)[idx_sort]
        temp_y = np.array(temp_y)[idx_sort]
        temp_z = np.array(temp_z)[idx_sort]

        temp_x = temp_x[temp_x!=0]
        temp_y = temp_y[temp_y!=0]    
        temp_z = temp_z[::2]
        temp_ene = temp_ene[::2]
        hit_tkr_ene.extend(temp_ene.tolist())
        hit_tkr_xpos.extend(temp_x.tolist())
        hit_tkr_ypos.extend(temp_y.tolist())
        hit_tkr_zpos.extend(temp_z.tolist())

    return np.array(hit_tkr_ene), np.array(hit_tkr_xpos), np.array(hit_tkr_ypos), np.array(hit_tkr_zpos)

def get_csi_hits(csi_ene_arr, csi_pos_arr):
    """ Read in the CSI data and return positon.

    This is called for each event id.

    Parameters
    ----------
    csi_ene_arr : np.array
        Energy Array of shape (30)
    csi_pos_arr : np.array
        Position Array of shape (30)

    Returns
    -------

    """
    hit_csi_ene = []
    hit_csi_xpos = []
    hit_csi_ypos = []
    hit_csi_zpos = []

    rowPos = np.linspace(0, 5, 6) * 1.9
    rowPos = rowPos - np.mean(rowPos)

    layerPos = np.linspace(0, 4, 5) * 1.9
    layerPos = layerPos-np.max(layerPos)-11.172-1.67/2

    for layer in range(0, 5):
        for row in range(0, 6):
            logNumber = layer * 6 + row
            if csi_ene_arr[logNumber] > CSI_SOFT_THRS:
                if (layer % 2 == 0):
                    xpos = float(-csi_pos_arr[logNumber])/10.0
                    ypos = rowPos[row]
                else:
                    xpos = rowPos[row]
                    ypos = float(-csi_pos_arr[logNumber])/10.0

                zpos = layerPos[4 - layer]
                ene = csi_ene_arr[logNumber]

                hit_csi_ene.append(float(ene))
                hit_csi_xpos.append(float(xpos))
                hit_csi_ypos.append(float(ypos))
                hit_csi_zpos.append(float(zpos))


    return np.array(hit_csi_ene), np.array(hit_csi_xpos), np.array(hit_csi_ypos), np.array(hit_csi_zpos)

def get_czt_hits(czt_ene_arr, czt_pos_arr):
    """ Get CZT hit information for each event id.

    This is called for each event id.

    Parameters
    ----------
    czt_ene_arr : Fltarr
        Event-sorted energy array. Shape (16, 16)
    czt_pos_arr : Fltarr
        Event-sorted energy array. Shape (16,16,3)

    Returns
    -------

    """

    '''For a single event, this function filters the CZT energy array into
    an energy array for encompassing all hits in the CZT for the event. 
    Information is then read from the position array and manipulated through
    geometric functions to obtain the coordinate position.

    Keyword Arguments:
    czt_ene_arr: Event-sorted energy array in h5 format ([16, 16])
    czt_pos_arr: Event-sorted energy array in h5 format ([16, 16, 3])
   
    Returns:    
    hit_czt_(ene, xpos, ypos, zpos) = array of hit info for the event

    Steps through a CZT block, then looks at the information from each bar for 
    the selected event. If the element is above the energy threshold the energy 
    is appended to the hit_czt_ene array. The (x,y,z) position is read from the
    position array, which holds position information relative to the center of
    the bar. This position information is used to obtain the global coordinate
    position geometrically.

    '''


    hit_czt_ene = []
    hit_czt_xpos = []
    hit_czt_ypos = []
    hit_czt_zpos = []
    [a,b] = czt_ene_arr.shape
    for block in np.arange(a):
        for bar in np.arange(b):
            if czt_ene_arr[block, bar] > CZT_SOFT_THRS:
                ene = czt_ene_arr[block, bar]

                CZT_YBar = int(bar/4)
                CZT_XBar = bar - 4*CZT_YBar
           
                CZT_YBlock = int(block/4)
                CZT_XBlock = block - 4*CZT_YBlock
                CZT_XBlock_mid = CZT_block_spacing*CZT_XBlock - CZT_edge_midpoint
                CZT_YBlock_mid = CZT_edge_midpoint-CZT_block_spacing*CZT_YBlock

                # --- SW: Updating these for current measurements.

                zpos = -4.68 # czt_pos_arr[block, bar, 2] Bandaid fix for now.

                CZT_Xrel = czt_pos_arr[block, bar, 0]/10.0
                xpos = CZT_Xrel#  + CZT_XBlock_mid - CZT_bar_edge + CZT_bar_spacing/2 + CZT_bar_spacing*(CZT_XBar)
               
                CZT_Yrel = czt_pos_arr[block, bar, 1]/10.0
                ypos = CZT_Yrel #+ CZT_YBlock_mid + CZT_bar_edge - CZT_bar_spacing/2 - CZT_bar_spacing*(CZT_YBar)
                #print(f'Ypos {ypos}, yrel{CZT_Yrel}')

                
                hit_czt_ene.append(float(ene))
                hit_czt_xpos.append(float(xpos))
                hit_czt_ypos.append(float(ypos))
                hit_czt_zpos.append(float(zpos))
    return np.array(hit_czt_ene),np.array(hit_czt_xpos), np.array(hit_czt_ypos), np.array(hit_czt_zpos)

def main(infile="", outfile="", verbose =False):

    with h5py.File(infile, "r") as f_cud:


        n_evts = f_cud['Events/TKR/EventID'].shape[0]

        Trk_nrg = f_cud["Events/TKR/Energy"][...]
        CZT_nrg = f_cud["Events/CZT/Energy"][...]
        CZT_pos = f_cud["Events/CZT/Position"][...]

        CsI_nrg = f_cud["Events/CsI/erg"][...]
        CsI_pos = f_cud["Events/CsI/pos"][...]

        hit_enes = []  # Define a list of hit energies, each input is an array
        hit_xpos = []
        hit_ypos = []
        hit_zpos = []

        Time = f_cud["Events/CZT/CorUTC"][...] / 1e9 #np.array(Events.get('Summary').get('UTC')) / 1e9

        for i_evt in range(n_evts):
            # for each event id:
            hit_tkr_ene, hit_tkr_x, hit_tkr_y, hit_tkr_z = get_tracker_hits(Trk_nrg[i_evt, ...])

            hit_csi_ene, hit_csi_x, hit_csi_y, hit_csi_z = get_csi_hits(CsI_nrg[i_evt, ...],
                                                                        CsI_pos[i_evt, ...])

            hit_czt_ene, hit_czt_x, hit_czt_y, hit_czt_z = get_czt_hits(CZT_nrg[i_evt, ...],
                                                                        CZT_pos[i_evt, ...])


            # Define the number of hits in each subsystem for the event
            n_hit_tkr = hit_tkr_ene.size
            n_hit_czt = hit_czt_ene.size
            n_hit_csi = hit_csi_ene.size

            this_n_hits = n_hit_tkr + n_hit_csi + n_hit_czt  # total number of hits for the event

            this_hit_ene = np.zeros(this_n_hits)  # define an array to store all hit energies
            this_hit_xpos = np.zeros(this_n_hits)  # define an array to store all x positions
            this_hit_ypos = np.zeros(this_n_hits)  # define an array to store all y positions
            this_hit_zpos = np.zeros(this_n_hits)  # define an array to store all z positions

            # Populate the main arrays with each subsytem data
            # Each hit would be a hit information.
            idx_0 = 0  # Define an arbitrary index
            idx_f = n_hit_tkr  # Limit the index to the number of events in the tkr
            this_hit_ene[idx_0:idx_f] = hit_tkr_ene
            this_hit_xpos[idx_0:idx_f] = hit_tkr_x
            this_hit_ypos[idx_0:idx_f] = hit_tkr_y
            this_hit_zpos[idx_0:idx_f] = hit_tkr_z

            idx_0 = idx_f
            idx_f = idx_0 + n_hit_czt
            this_hit_ene[idx_0:idx_f] = hit_czt_ene
            this_hit_xpos[idx_0:idx_f] = hit_czt_x
            this_hit_ypos[idx_0:idx_f] = hit_czt_y
            this_hit_zpos[idx_0:idx_f] = hit_czt_z

            idx_0 = idx_f
            idx_f = idx_0 + n_hit_csi
            this_hit_ene[idx_0:idx_f] = hit_csi_ene
            this_hit_xpos[idx_0:idx_f] = hit_csi_x
            this_hit_ypos[idx_0:idx_f] = hit_csi_y
            this_hit_zpos[idx_0:idx_f] = hit_csi_z

            # Import the arrays into the lists
            hit_enes += [this_hit_ene]
            hit_xpos += [this_hit_xpos]
            hit_ypos += [this_hit_ypos]
            hit_zpos += [this_hit_zpos]

        # Define an index for the total number of hits
        # Needed for evta parameters.
        n_hits_tot = 0

        # Iterate over each array in the list of hit energies
        for hit_arr in hit_enes:
            n_hits_tot += hit_arr.size
        # Define an array of all hit energies
        all_hit_enes = np.zeros(n_hits_tot)

        idx_0 = 0
        for this_evt_hit_enes in hit_enes:
            idx_f = idx_0 + this_evt_hit_enes.size
            all_hit_enes[idx_0:idx_f] = this_evt_hit_enes
            idx_0 = idx_f

        # Define an array of all x positions
        all_hit_xpos = np.zeros(n_hits_tot)

        idx_0 = 0
        for this_evt_hit_xpos in hit_xpos:
            idx_f = idx_0 + this_evt_hit_xpos.size
            all_hit_xpos[idx_0:idx_f] = this_evt_hit_xpos
            idx_0 = idx_f
        # Define an array of all y positions
        all_hit_ypos = np.zeros(n_hits_tot)

        idx_0 = 0
        for this_evt_hit_ypos in hit_ypos:
            idx_f = idx_0 + this_evt_hit_ypos.size
            all_hit_ypos[idx_0:idx_f] = this_evt_hit_ypos
            idx_0 = idx_f
        # Define an array of all z positions
        all_hit_zpos = np.zeros(n_hits_tot)

        idx_0 = 0
        for this_evt_hit_zpos in hit_zpos:
            idx_f = idx_0 + this_evt_hit_zpos.size
            all_hit_zpos[idx_0:idx_f] = this_evt_hit_zpos
            idx_0 = idx_f

        idx_0 = 0
        # Define an array of all hit IDs
        all_hit_eids = np.zeros(n_hits_tot)
        for i_evt, these_enes in enumerate(hit_enes):  # enumerate also returns a dummy index
            idx_f = idx_0 + these_enes.size
            all_hit_eids[idx_0:idx_f] = i_evt
            idx_0 = idx_f

        #Hits = data.get('Hits')
        det = 0

        HitID = all_hit_eids
        Position = [all_hit_xpos, all_hit_ypos, all_hit_zpos]
        Energy = all_hit_enes

        # File writing starts here.
        with open(outfile, 'w') as outf1:
            # HEADER
            outf1.write(f"#Revan Readable Converted File\n")
            outf1.write(f"#Created by cud2rr.py {CUD2RR_VERSION}\n")
            outf1.write(f"\n")

            outf1.write(f"Type      EVTA\n")
            outf1.write(f"Version   {MEGA_VERSION}\n")
            outf1.write(f"\n")

            # EVENT
            for i in range(0, n_hits_tot):
                if 0 <= all_hit_zpos[i] <= 18.00:
                    det = 1
                if -9.05 < all_hit_zpos[i] < -1.1:
                    det = 7
                if -22.68 <= all_hit_zpos[i] <= -9.05:
                    det = 2
                if int(i) == 0:
                    outf1.write('SE' + '\n')  # mandatory.
                    outf1.write('ID ' + str(int(HitID[i] + 1)) + '\n')
                    outf1.write('TI ' + str(Time[int(HitID[i])]) + '\n')
                    outf1.write( f"HTsim {det};   {all_hit_xpos[i]:.5f};   {all_hit_ypos[i]:.5f};   {all_hit_zpos[i]:.5f};  {Energy[i]:.5f}\n")
                    #outf1.write('HTsim ' + str(det) + '; ' + str(all_hit_xpos[i]) + '; ' + str(all_hit_ypos[i]) + '; ' + str(all_hit_zpos[i]) + '; ' + str(Energy[i]) + '\n')
                else:
                    if int(HitID[i]) != int(HitID[i - 1]):
                        outf1.write('SE' + '\n')
                        outf1.write('ID ' + str(int(HitID[i] + 1)) + '\n')
                        outf1.write('TI ' + str(Time[int(HitID[i])]) + '\n')
                        outf1.write( f"HTsim {det};   {all_hit_xpos[i]:.5f};   {all_hit_ypos[i]:.5f};   {all_hit_zpos[i]:.5f};   {Energy[i]:.5f} \n")
                        #outf1.write('HTsim ' + str(det) + '; ' + str(all_hit_xpos[i]) + '; ' + str(all_hit_ypos[i]) + '; ' + str(all_hit_zpos[i]) + '; ' + str(Energy[i]) + '\n')
                    else:
                        #outf1.write('HTsim ' + str(det) + '; ' + str(all_hit_xpos[i]) + '; ' + str(all_hit_ypos[i]) + '; ' + str(all_hit_zpos[i]) + '; ' + str(Energy[i]) + '\n')
                        outf1.write( f"HTsim {det};\t{all_hit_xpos[i]:.5f};\t{all_hit_ypos[i]:.5f};\t{all_hit_zpos[i]:.5f};\t{Energy[i]:.5f}\n")

            outf1.write('\n' + 'EN')
        if verbose:
            print('--- Energy info decoded.')



if __name__ == "__main__":
    # Default output set to Cosima.evta

    parser = argparse.ArgumentParser(description="Parse the h5 file to get the Revan Readable")
    parser.add_argument("infile", help="Path to input *cud.h5 file", default="empty")

    args = parser.parse_args()

    infile = args.infile
    outfile= f"{infile.split('.CUD.h5')[0]}.evta"

    main(infile=infile, outfile=outfile , verbose = True)
